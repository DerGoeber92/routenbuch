class ApiController < ActionController::API
  before_action :authenticate_user! unless Routenbuch.public?
  check_authorization

  before_action :check_access_scope
  def check_access_scope
    return if current_user.nil?

    authorize! :scope, access_scope
  end

  def access_scope
    raise CanCan::AccessDenied, 'no access_scope defined'
  end

  rescue_from CanCan::AccessDenied do |exception|
    render json: { error: exception.message }, status: :forbidden
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render json: { error: exception.message }, status: :not_found
  end

  private

  def jsonapi_paginate_links(collection)
    links = {}
    links[:self] = request.original_url
    links[:next] = url_for params: { page: collection.next_page } unless collection.next_page.nil?
    links[:prev] = url_for params: { page: collection.prev_page } unless collection.prev_page.nil?
    links
  end

  def jsonapi_paginate_meta(collection)
    {
      total: collection.total_count
    }
  end
end
