class SeasonClosuresController < ApplicationController
  before_action :set_closure, only: [:new, :create]
  before_action :set_season_closure, only: [:edit, :update, :destroy]
  
  def new
    @season_closure = SeasonClosure.new
    @season_closure.closure = @closure
    @season_closure.year = Date.today.year
    date, time, desc = if @closure.active?
                         ['end', 1.day.ago, _('Early deregulation')]
                       else
                         ['start', Time.now, _('Extraordinary closure')]
                       end
    @season_closure.send("#{date}_day_of_month=", time.day)
    @season_closure.send("#{date}_month=", time.month)
    @season_closure.description = desc
    authorize! :create, @season_closure
  end

  def edit
    authorize! :update, @season_closure
  end

  def create
    @season_closure = SeasonClosure.new(season_closure_params)
    @season_closure.closure = @closure
    authorize! :create, @season_closure
    if @season_closure.save
      redirect_to @closure, notice: _('SeasonClosure was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @season_closure
    if @season_closure.update(season_closure_params)
      redirect_to @season_closure.closure, notice: _('SeasonClosure was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @season_closure
    @season_closure.destroy
    redirect_to @season_closure.closure, notice: _('SeasonClosure was successfully destroyed.')
  end

  private

  def set_closure
    @closure = Closure.find(params[:closure_id])
  end

  def set_season_closure
    @season_closure = SeasonClosure.find(params[:id])
  end

  def season_closure_params
    params.require(:season_closure).permit(
      :description,
      :year,
      :start_month,
      :start_day_of_month,
      :end_month,
      :end_day_of_month,
    )
  end
end
