class Admin::MonitoringController < Admin::ApplicationController
  def index
    @result = {}
    HealthCheck.full_checks.each do |check|
      @result[check] = HealthCheck::Utils.process_checks([check])
    end
  end
end
