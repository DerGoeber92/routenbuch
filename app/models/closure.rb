# == Schema Information
#
# Table name: closures
#
#  id                         :bigint           not null, primary key
#  active                     :boolean          default(FALSE), not null
#  active_changed_at          :date
#  days_closed                :integer
#  description                :text             default(""), not null
#  end_at                     :date
#  kind                       :string           default("flexible"), not null
#  name                       :string           default(""), not null
#  regular_days_closed        :integer
#  regular_end_day_of_month   :integer
#  regular_end_month          :integer
#  regular_start_day_of_month :integer
#  regular_start_month        :integer
#  start_at                   :date
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  closure_reason_id          :bigint
#  geo_ref_id                 :bigint
#
# Indexes
#
#  index_closures_on_closure_reason_id  (closure_reason_id)
#  index_closures_on_geo_ref_id         (geo_ref_id)
#
# Foreign Keys
#
#  fk_rails_...  (closure_reason_id => closure_reasons.id)
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#
class Closure < ApplicationRecord
  class << self
    def valid_geo_ref_types
      [Country, Region].freeze
    end

    def valid_geo_ref_type?(klass)
      valid_geo_ref_types.include? klass
    end

    def kinds
      [
        N_('fixed'),
        N_('flexible'),
        N_('temporary'),
      ].freeze
    end
  end

  has_many :season_closures, dependent: :delete_all
  has_many :relevant_season_closures,
    -> { year = Time.now.year ; where year: [(year-2), (year-1), year] },
    class_name: 'SeasonClosure'
  belongs_to :geo_ref
  has_and_belongs_to_many :geo_refs
  has_and_belongs_to_many :routes
  belongs_to :closure_reason, optional: true

  validates :name, presence: true
  validates :regular_start_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  validates :regular_start_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  validates :regular_end_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  validates :regular_end_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  validates :kind,
    inclusion: {
      in: Closure.kinds
    }

  include RbidIdentifiable

  def self.icon
    'lock'
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[
      name
      description
      kind
      regular_start_month
      regular_start_day_of_month
      regular_end_month
      regular_end_day_of_month
      start_at
      end_at
      active
      active_changed_at
      days_closed
      regular_days_closed
      geo_ref_id
    ]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[geo_ref]
  end

  class Calc
    def initialize(closure, date, use_relevant_season_closures)
      @closure = closure
      @date = date
      @use_relevant_season_closures = use_relevant_season_closures
    end

    def start_year
      @start_year ||= if multi_year? && !after_end?
                        @date.year - 1
                      else
                        @date.year
                      end
    end

    def end_year
      @end_year ||= if multi_year? && after_end?
                      @date.year + 1
                    else
                      @date.year
                    end
    end

    def after_end?
      build_date(@date.year, @closure.regular_end_month, @closure.regular_end_day_of_month) < @date
    end

    def multi_year?
      return false if @closure.regular_start_month.nil? \
        || @closure.regular_end_month.nil? \
        || @closure.regular_start_day_of_month.nil? \
        || @closure.regular_end_day_of_month.nil?

      build_date(0, @closure.regular_start_month, @closure.regular_start_day_of_month) > \
        build_date(0, @closure.regular_end_month, @closure.regular_end_day_of_month)
    end

    def season_closure
      return @season_closure if defined? @season_closure

      @season_closure = \
        if @use_relevant_season_closures && @closure.relevant_season_closures.loaded?
          @closure.relevant_season_closures.select { |sc| sc.year == start_year }.first
        elsif @closure.season_closures.loaded?
          @closure.season_closures.select { |sc| sc.year == start_year }.first
        else
          @closure.season_closures.where(year: start_year).first
        end
    end

    def start_month
      @start_month ||= season_closure&.start_month || @closure.regular_start_month
    end

    def start_day_of_month
      @start_day_of_month ||= season_closure&.start_day_of_month || @closure.regular_start_day_of_month
    end

    def end_month
      @end_month ||= season_closure&.end_month || @closure.regular_end_month
    end

    def end_day_of_month
      @end_day_of_month ||= season_closure&.end_day_of_month || @closure.regular_end_day_of_month
    end

    def start_at
      build_date(start_year, start_month, start_day_of_month)
    end

    def regular_start_at
      build_date(start_year, @closure.regular_start_month, @closure.regular_start_day_of_month)
    end

    def end_at
      build_date(end_year, end_month, end_day_of_month)
    end

    def open_at
      end_date = end_at
      return if end_date.nil?

      end_date + 1
    end

    def regular_end_at
      build_date(end_year, @closure.regular_end_month, @closure.regular_end_day_of_month)
    end

    def regular_open_at
      end_date = regular_end_at
      return if end_date.nil?

      end_date + 1
    end

    def active?
      return false if start_at.nil? || end_at.nil?

      @date >= start_at && @date <= end_at
    end

    def complete?
      return false if end_at.nil? || start_at.nil?

      true
    end

    def regular_days_closed
      return if regular_start_at.nil? || regular_open_at.nil?

      (regular_open_at - regular_start_at).to_i
    end

    def days_closed
      return if start_at.nil? || open_at.nil?

      (open_at - start_at).to_i
    end

    protected

    def build_date(year, month, day)
      return if month.nil? || day.nil?

      # use last day of month if day exceeds month days
      last_day_of_month = Date.new(year, month, 1).next_month.prev_day.mday
      Date.new(year, month, day > last_day_of_month ? last_day_of_month : day)
    end
  end

  def calc_at(date, use_relevant_season_closures = false)
    Calc.new(self, date, use_relevant_season_closures)
  end

  def current
    @current ||= calc_at(Date.today, true)
  end

  def prev_year
    @prev_year ||= calc_at(Date.today.prev_year, true)
  end

  def reset_calculations_cache
    @current = @prev_year = nil
  end

  def active_changed_at!
    change_dates = []
    change_dates += [current.open_at, current.start_at] if current.complete?
    change_dates << prev_year.open_at if prev_year.complete?

    today = Date.today
    change_dates.reject {|d| d > today }.first
  end

  before_save :update_cached_attributes
  def update_cached_attributes
    self.end_at = current.end_at
    self.start_at = current.start_at
    self.active = current.active?
    self.active_changed_at = active_changed_at!
    self.days_closed = current.days_closed
    self.regular_days_closed = current.regular_days_closed
  end

  def apply_template(template)
    %i[
      closure_reason
      regular_start_month
      regular_start_day_of_month
      regular_end_month
      regular_end_day_of_month
    ].each do |field|
      send("#{field}=", template.send(field))
    end
  end

  has_paper_trail(
    ignore: %i[end_at start_at active active_changed_at],
    meta: {
      geo_ref_id: :geo_ref_id
    }
  )
end
