module Diff
  module LCS
    class RoutenbuchDiffCallbacks
      attr_reader :diffs

      def initialize
        @hunk = []
        @diffs = []
        @current_mode = nil 

        return unless block_given?

        begin
          yield self
        ensure
          finish
        end
      end

      def append(mode, element)
        @current_mode = mode if @current_mode.nil?
        if @current_mode != mode
          finish_hunk
          @current_mode = mode
        end
        @hunk << element
      end

      def finish
        finish_hunk
      end

      def match(event)
        append('=', event.old_element)
      end

      def discard_a(event)
        append('-', event.old_element)
      end

      def discard_b(event)
        append('+', event.new_element)
      end

      def finish_hunk
        @diffs << [@current_mode, @hunk]
        @hunk = []
      end
      private :finish_hunk
    end
  end
end
