FactoryBot.define do
  factory :rbid do
    item factory: :crag
    hint { 'test crag' }
  end
end
