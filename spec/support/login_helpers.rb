module LoginHelpers
  def login_user(user_factory)
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[user_factory] unless @request.nil?
      @user = FactoryBot.create(user_factory)
      sign_in @user
    end
  end

  def with_user(users, &block)
    users = Array.wrap(users)
    users.each do |user|
      context "with user #{user}" do
        login_user user
        class_exec(&block)
      end
    end
  end
end
