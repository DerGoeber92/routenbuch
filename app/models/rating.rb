# == Schema Information
#
# Table name: ratings
#
#  id                 :bigint           not null, primary key
#  average_score      :float
#  target_type        :string
#  user_ratings_count :integer          default(0), not null
#  target_id          :integer
#
# Indexes
#
#  index_ratings_on_target_type_and_target_id  (target_type,target_id) UNIQUE
#
class Rating < ApplicationRecord
  def self.icon
    'star'
  end

  belongs_to :target, polymorphic: true
  has_many :user_ratings

  validates :average_score, score: true

  def self.update_average_scores
    update_sql = UserRating.select('AVG(user_ratings.score)')
                           .where('user_ratings.rating_id = ratings.id')
                           .to_sql
    update_all("average_score = (#{update_sql})")
  end

  def update_average_score
    self.class.unscoped.where(self.class.primary_key => id).update_average_scores
    reload
  end
end
