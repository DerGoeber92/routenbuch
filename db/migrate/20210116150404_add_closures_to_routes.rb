class AddClosuresToRoutes < ActiveRecord::Migration[6.1]
  def change
    create_table :closures_routes, id: false do |t|
      t.bigint :closure_id
      t.bigint :route_id
    end

    add_index :closures_routes, :closure_id
    add_index :closures_routes, :route_id
    add_index :closures_routes, [:closure_id, :route_id], unique: true
  end
end
