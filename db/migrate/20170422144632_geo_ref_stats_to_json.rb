class GeoRefStatsToJson < ActiveRecord::Migration[5.0]
  def change
    add_column :geo_refs, :stats_new, :json
    GeoRef.all.each do |geo|
      geo.stats_new = geo.stats
      geo.save
    end
    change_table :geo_refs do |t|
      t.remove :stats
      t.rename :stats_new, :stats
    end
  end
end
