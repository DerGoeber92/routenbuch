# == Schema Information
#
# Table name: ascents
#
#  id         :integer          not null, primary key
#  attempts   :integer
#  comment    :text
#  pitches    :integer
#  when       :date
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  route_id   :integer
#  style_id   :integer
#  user_id    :integer
#
# Indexes
#
#  index_ascents_on_route_id  (route_id)
#  index_ascents_on_style_id  (style_id)
#  index_ascents_on_user_id   (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (route_id => routes.id)
#  fk_rails_...  (style_id => styles.id)
#  fk_rails_...  (user_id => users.id)
#
class Ascent < ApplicationRecord
  belongs_to :user
  belongs_to :route
  belongs_to :style

  validates :when, presence: true
  validates :attempts, presence: true

  after_commit do
    user.after_ascent_commit(self)
  end

  def self.icon
    'address-book-o'
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[when attempts]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[route style]
  end
end
