class AddPublicFlags < ActiveRecord::Migration[5.1]
  def change
    add_column :geo_refs, :public, :boolean, null: false, default: true
  end
end
