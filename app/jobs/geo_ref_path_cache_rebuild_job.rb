# update GeoRefPathCache for all GeoRef by iterating over all top-level
# locations/countries
class GeoRefPathCacheRebuildJob < ApplicationJob
  queue_as :default

  def perform
    GeoRef.top_level.each do |geo_ref|
      GeoRefPathCacheService.update(geo_ref)
    end
  end
end
