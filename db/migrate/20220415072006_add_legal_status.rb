class AddLegalStatus < ActiveRecord::Migration[6.1]
  def change
    add_column :geo_refs, :legal_status, :string
    add_column :routes, :legal_status, :string

    add_index :geo_refs, :legal_status
    add_index :routes, :legal_status
  end
end
