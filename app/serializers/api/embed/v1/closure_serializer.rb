module Api
  module Embed
    module V1
      class ClosureSerializer
        include JSONAPI::Serializer

        attributes :name, :description, :kind, :active_changed_at
        attribute :reason do |c|
          c.closure_reason&.name
        end
        attribute :active, &:active?
        attribute :season_description do |c|
          c.current&.season_closure&.description
        end

        %i[start_at end_at regular_start_at regular_end_at].each do |attr|
          attribute attr do |c|
            c.current.send(attr)
          end
        end

        attribute :region do |c|
          [ c.geo_ref.name ] + c.geo_ref&.parents&.map(&:name)
        end

        attribute :geo_refs do |c, params|
          geo_refs = params[:geo_refs][c.id] || []
          geo_refs.map do |g|
            {
              id: g.id.to_s,
              type: g.type,
              name: g.name,
              alternative_names: g.alternative_names,
              route_first: g.route_first&.name,
              route_last: g.route_last&.name,
            }
          end
        end

        attribute :routes do |c, params|
          routes = params[:routes][c.id] || []
          routes.map do |g|
            { id: g.id.to_s, name: g.name, alternative_names: g.alternative_names }
          end
        end

        meta do |c|
          { updated_at: c.updated_at }
        end
      end
    end
  end
end
