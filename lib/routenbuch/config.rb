require "yaml"
require "erb"

module Routenbuch
  class Config < ActiveSupport::HashWithIndifferentAccess
    class MissingSetting < StandardError; end

    attr_accessor :section

    attr_writer :env
    def env
      return @env if defined? @env

      @env = Rails.env
    end

    def load_file(file)
      raw = File.read(file)
      content = ERB.new(raw).result
      hash = YAML.safe_load(content, aliases: true).to_hash
      self.replace(hash[env])
    end

    def method_missing(name, *args, &block)
      raise MissingSetting, "No setting for #{name} in section #{section}" \
        unless key? name

      value = fetch(name)
      case value
      when Hash
        sub_config = self.class.new(value)
        sub_config.section = name
        self[name] = sub_config
      else
        value
      end
    end
  end
end
