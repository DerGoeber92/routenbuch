class ErrorsController < ApplicationController
  skip_authorization_check
  back_navigatable_actions []

  def access_scope
    :public
  end

  %i[
    forbidden
    not_found
    unprocessable_entity
    internal_server_error
  ].each do |error|
    define_method error do
      render error
    end
  end
end
