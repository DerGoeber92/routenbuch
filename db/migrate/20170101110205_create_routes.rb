class CreateRoutes < ActiveRecord::Migration[5.0]
  def change
    create_table :routes do |t|
      t.string :name
      t.text :body
      t.references :grade, foreign_key: true
      t.references :first_ascent_person, foreign_key: true

      t.timestamps
    end
  end
end
