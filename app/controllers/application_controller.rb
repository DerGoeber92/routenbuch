class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception, prepend: true

  before_action :authenticate_user! unless Routenbuch.public?
  check_authorization unless: :devise_controller?
  # HACK: to internal @_authorized used by cancancan
  def skip_authorization_check(*_args)
    @_authorized = true
  end

  layout :user_theme

  include BackNavigatable

  before_action :set_locale
  def set_locale
    if current_user&.locale.present?
      FastGettext.set_locale(current_user.locale.gsub('-', '_'))
      I18n.locale = current_user.locale.to_sym
    else
      set_gettext_locale
    end

    FastGettext.reload! if Rails.env.development?
  end

  before_action :check_access_scope
  def check_access_scope
    # login, logout, etc.
    return if devise_controller?
    # error pages, etc.
    return if access_scope == :public
    # guest users
    return if current_user.nil?

    authorize! :scope, access_scope
  end

  def access_scope
    :web_ui
  end

  # set user on versions
  before_action :set_paper_trail_whodunnit
  def info_for_paper_trail
    user_id = current_user.id if current_user.is_a? User
    {
      user_id: user_id
    }
  end

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      format.html { redirect_to forbidden_path, notice: exception.message }
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end

  def not_found
    raise ActionController::RoutingError, 'Not Found'
  end

  helper_method :navigation_context, :show_global_search, :show_navigation, :page_title, :logo_url, :custom_css_url

  private

  def page_title
    Routenbuch.instance_name
  end

  def navigation_context
    'default'
  end

  def show_global_search
    show_navigation
  end

  def show_navigation
    return false if !Routenbuch.public? && current_user.nil?

    true
  end

  def logo_url
    Routenbuch.config[:logo_url]
  end

  def custom_css_url
    Routenbuch.config[:custom_css_url]
  end

  def user_theme
    return current_user.theme \
      if UserThemeValidator.valid?(current_user&.theme)
    return Routenbuch.config[:default_theme] \
      if UserThemeValidator.valid?(Routenbuch.config[:default_theme])

    'twentytwo'
  end
end
