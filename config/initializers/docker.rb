# autoflush stdout when running in container
if File.exist?("/.dockerenv")
  $stdout.sync = true
end
