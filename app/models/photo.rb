# == Schema Information
#
# Table name: photos
#
#  id          :integer          not null, primary key
#  cover_photo :boolean          default(FALSE), not null
#  description :string
#  pinned      :boolean          default(FALSE), not null
#  private     :boolean          default(FALSE), not null
#  target_type :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  geo_ref_id  :bigint           not null
#  target_id   :integer
#  user_id     :bigint           not null
#
# Indexes
#
#  index_photos_on_cover_photo                (cover_photo)
#  index_photos_on_geo_ref_id                 (geo_ref_id)
#  index_photos_on_pinned                     (pinned)
#  index_photos_on_private                    (private)
#  index_photos_on_target_type_and_target_id  (target_type,target_id)
#  index_photos_on_user_id                    (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#  fk_rails_...  (user_id => users.id)
#
class Photo < ApplicationRecord
  validates_presence_of :photo, :description, :target
  belongs_to :target, polymorphic: true
  belongs_to :user
  belongs_to :geo_ref

  include TargetAccessFilterable

  has_one_attached :photo
  validates(
    :photo,
    content_type: Routenbuch.supported_image_types,
    size: { less_than: Routenbuch.max_upload_size }
  )

  scope(
    :cover_photos,
    lambda do
      where(cover_photo: true).order(Arel.sql('RANDOM()'))
    end
  )

  scope(
    :ids_for_geo_refs_by_groups,
    lambda do |group_ids|
      select(:id) \
        .where(target_type: 'GeoRef') \
        .joins('JOIN geo_refs ON geo_refs.id = target_id') \
        .joins('JOIN geo_refs_groups ON geo_refs_groups.geo_ref_id = geo_refs.id') \
        .where('geo_refs_groups.group_id' => group_ids).distinct
    end
  )

  scope(
    :ids_for_routes_by_groups,
    lambda do |group_ids|
      select(:id) \
        .where(target_type: 'Route') \
        .joins('JOIN routes ON routes.id = target_id') \
        .joins('JOIN geo_refs ON routes.geo_ref_id = geo_refs.id') \
        .joins('JOIN geo_refs_groups ON geo_refs_groups.geo_ref_id = geo_refs.id') \
        .where('geo_refs_groups.group_id' => group_ids).distinct
    end
  )

  scope(
    :all_of_groups,
    lambda do |group_ids|
      union_sql = [
        ids_for_geo_refs_by_groups(group_ids),
        ids_for_routes_by_groups(group_ids),
      ].map do |relation|
        relation.to_sql
      end.join(' UNION ')

      where("photos.id IN (#{union_sql})")
    end
  )

  def self.icon
    'photo'
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[description]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end
end
