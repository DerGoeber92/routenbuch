class RegulatedGeoRefsController < ApplicationController
  before_action :set_regulated_geo_ref, only: %i[edit update destroy]
  before_action :set_regulation, only: %i[new create]

  def new
    @regulated_geo_ref = RegulatedGeoRef.new
    @regulated_geo_ref.regulation = @regulation
    authorize! :create, @regulated_geo_ref
  end

  def edit
    authorize! :update, @regulated_geo_ref
  end

  def create
    @regulated_geo_ref = RegulatedGeoRef.new(regulated_geo_ref_params)
    @regulated_geo_ref.regulation = @regulation
    authorize! :create, @regulated_geo_ref

    if @regulated_geo_ref.save
      redirect_to regulation_path(@regulated_geo_ref.regulation), notice: _('Regulated location has been added.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @regulated_geo_ref
    if @regulated_geo_ref.update(regulated_geo_ref_params)
      redirect_to regulation_path(@regulated_geo_ref.regulation), notice: _('Regulated location was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @regulated_geo_ref
    @regulated_geo_ref.destroy
    redirect_to regulation_path(@regulated_geo_ref.regulation), notice: _('Regulated location was successfully removed.')
  end

  private

  def set_regulation
    @regulation = Regulation.find(params[:regulation_id])
    authorize! :read, @regulation
  end

  def set_regulated_geo_ref
    @regulated_geo_ref = RegulatedGeoRef.find(params[:id])
  end

  def regulated_geo_ref_params
    params.require(:regulated_geo_ref).permit(:geo_ref_id, :reason)
  end
end
