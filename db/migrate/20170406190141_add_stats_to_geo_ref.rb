class AddStatsToGeoRef < ActiveRecord::Migration[5.0]
  def change
    add_column :geo_refs, :stats, :text
  end
end
