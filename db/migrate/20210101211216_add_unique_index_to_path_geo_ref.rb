class AddUniqueIndexToPathGeoRef < ActiveRecord::Migration[6.1]
  def change
    add_index :geo_refs_paths, [:geo_ref_id, :path_id], unique: true
  end
end
