class HelpController < ApplicationController
  before_action do
    authorize! :read, :help
  end

  def show
    respond_to do |format|
      format.any(:markdown, :md, :html) do
        path = filesystem_path('md')
        if File.exist?(path)
          @markdown = File.read(path)

          render
        else
          not_found
        end
      end

      format.any(:png, :gif, :jpeg) do
        path = filesystem_path(params['format'])
        if File.exist?(path)
          send_file(path, disposition: 'inline')
        else
          head :not_found
        end
      end

      format.any { head :not_found }
    end
  end

  private

  def filesystem_path(format = 'md')
    sanitized_path = Rack::Utils.clean_path_info(path_params[:path])
    File.join(Rails.root, 'help', "#{sanitized_path}.#{format}")
  end

  def path_params
    params.require(:path)
    params
  end
end
