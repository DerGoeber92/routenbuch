require 'rails_helper'

RSpec.describe DashboardController, type: :request do
  login_user :admin_user

  it 'index' do
    get dashboard_path
    expect(response).to have_http_status(:ok)
  end

  it 'mark_all_as_read' do
    put dashboard_mark_all_as_read_path
    expect(response).to have_http_status(:redirect)
  end
end
