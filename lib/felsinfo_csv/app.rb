require 'optparse'

module FelsinfoCsv
  class App
    def initialize(handle = nil)
      @handle = handle || ARGF
      parse_opts
    end

    def parse_opts
      OptionParser.new do |opts|
        opts.banner = "Usage: felsinfo-csv [options] [file]"

        opts.on("-lLEVEL", "--log_level=LEVEL", "Rails logger log_level") do |level|
          Rails.logger.level = level.to_sym
        end
      end.parse!
    end

    def run
      puts "running in rails env: #{Rails.env}"

      @handle.each_line.with_index do |line, line_number|
        next if line_number == 0
        next if line =~ /^\s*$/
        next if line =~ /^\#/

        record = parse_line(line)
        name = record[:name]
        name.gsub!(/[ (][^)]*[)]/, '')

        puts "#{line_number}: importing #{name}..."

        begin
          geo_ref = GeoRef.with_name(name).first
          if geo_ref.nil?
            puts "cannot find #{name}"
            next
          end

          if geo_ref.lat.present? || geo_ref.lng.present?
            puts "lat/lng already present"
          else
            puts "updating lat/lng from csv..."
            geo_ref.update!(
              lat: record[:lat].to_f,
              lng: record[:lng].to_f,
            )
          end
          
          if record[:published] != '1' && !['private', 'confidential'].include?(geo_ref.access_level)
            puts "is not published but not private or confidential!"
          end

          zone_names = ([geo_ref.zone&.name] + geo_ref.secondary_zones.map(&:name)).reject(&:nil?)

          missing_zones = record[:zones] - zone_names
          additional_zones = zone_names - record[:zones]
          if missing_zones.any? || additional_zones.any?
            puts "zones missing: #{missing_zones.join(', ')}"
            puts "zones additional: #{additional_zones.join(', ')}"
            puts "#{zone_names.inspect} (db) <-> #{record[:zones].inspect} (csv)"
          end
        rescue StandardError => e
          puts "failed to import line #{line_number + 1} (#{record[:geo_ref_name]}): #{e}"
        end
      end
    end

    def parse_line(line)
      line.chomp!
      fields = line.split('|')
      zones = []
      zones << "Zone 1" unless fields[5].blank?
      zones << "Zone 2" unless fields[6].blank?
      zones << "Zone 3" unless fields[7].blank?
      {
        name: fields[0],
        lng: fields[3],
        lat: fields[4],
        zones: zones,
        published: fields[8],
      }
    end
  end
end
