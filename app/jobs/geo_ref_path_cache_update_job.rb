# update GeoRefPathCache for a GeoRef
class GeoRefPathCacheUpdateJob < ApplicationJob
  queue_as :default

  def perform(geo_ref_id)
    geo_ref = GeoRef.find(geo_ref_id)
    return unless geo_ref

    GeoRefPathCacheService.update(geo_ref)
  end
end
