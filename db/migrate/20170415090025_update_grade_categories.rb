class UpdateGradeCategories < ActiveRecord::Migration[5.0]
  def change
    # yellow, 5+
    Grade.where("grades.difficulty < ?", 6).each do | grade |
      grade.category = 'very-easy'
      grade.save!
    end
    # green, 6+
    Grade.where("grades.difficulty BETWEEN 6 AND 10").each do | grade |
      grade.category = 'easy'
      grade.save!
    end
    # blau, 7+
    Grade.where("grades.difficulty BETWEEN 10 AND 13").each do | grade |
      grade.category = 'medium'
      grade.save!
    end
    # rot, 8+
    Grade.where("grades.difficulty BETWEEN 13 AND 17").each do | grade |
      grade.category = 'hard'
      grade.save!
    end
    # schwarz, 9+
    Grade.where("grades.difficulty BETWEEN 17 AND 20").each do | grade |
      grade.category = 'extrem'
      grade.save!
    end
    # sienna, 10- and up
    Grade.where("grades.difficulty > ?", 20).each do | grade |
      grade.category = 'ultra'
      grade.save!
    end
  end
end
