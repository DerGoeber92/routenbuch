class CreateRbidTable < ActiveRecord::Migration[6.1]
  def change
    create_table :rbids do |t|
      t.references :item, polymorphic: true, null: false
      t.string :hint, null: false, default: ''

      t.timestamps
    end
  end
end
