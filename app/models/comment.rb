# == Schema Information
#
# Table name: comments
#
#  id          :bigint           not null, primary key
#  body        :text             default(""), not null
#  deleted_at  :datetime
#  kind        :string           default("comment"), not null
#  pinned      :boolean          default(FALSE), not null
#  private     :boolean          default(FALSE), not null
#  target_type :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  geo_ref_id  :bigint           not null
#  target_id   :integer
#  user_id     :bigint
#
# Indexes
#
#  index_comments_on_geo_ref_id                 (geo_ref_id)
#  index_comments_on_kind                       (kind)
#  index_comments_on_pinned                     (pinned)
#  index_comments_on_private                    (private)
#  index_comments_on_target_type_and_target_id  (target_type,target_id)
#  index_comments_on_user_id                    (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#

require 'soft_deletion'

class Comment < ApplicationRecord
  class << self
    def kinds
      [
        N_('comment'),
        N_('beta'),
        N_('maintenance_required'),
        N_('maintenance'),
        N_('nature'),
        N_('access'),
        N_('wrong_information')
      ].freeze
    end

    def icon
      'comments'
    end
  end

  def icon
    'comment'
  end

  belongs_to :target, polymorphic: true
  belongs_to :user
  belongs_to :geo_ref

  include TargetAccessFilterable

  validates(
    :kind,
    inclusion: {
      in: Comment.kinds,
      message: '%{value} is not a valid type for comment'
    },
    presence: true
  )

  validates :body, length: { maximum: 4096 }

  has_soft_deletion

  before_soft_delete do
    self.body = ''
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[kind private body]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end

  def user_modified?
    created_at != updated_at
  end

  APPLIED_TAGS_BY_KIND = {
    'maintenance_required' => {
      add: ['maintenance required']
    },
    'maintenance' => {
      remove: ['maintenance required']
    },
    'wrong_information' => {
      add: ['needs review']
    }
  }.freeze

  def applied_tags
    APPLIED_TAGS_BY_KIND[kind] || {}
  end

  def apply_tags
    applied_tags.each do |action, fields|
      fields.each do |field|
        target.send("#{action}_tag", field) if target.supports_tag?(field)
      end
    end
  end
end
