class AddDaysClosedToClosures < ActiveRecord::Migration[6.1]
  def change
    add_column :closures, :days_closed, :integer, null: true
    add_column :closures, :regular_days_closed, :integer, null: true
  end
end
