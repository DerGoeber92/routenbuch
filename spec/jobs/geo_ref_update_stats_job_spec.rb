require "rails_helper"

RSpec.describe GeoRefUpdateStatsJob, :type => :job do
  let(:geo_ref) { create(:geo_ref) }

  describe "#perform" do
    it 'update a GeoRef#stats' do
      expect {
        subject.perform(geo_ref.id)
      }.not_to raise_error
    end
  end
end
