require 'rails_helper'

RSpec.describe DashboardTimestampService do
  let(:user) { create :user }
  let(:ds) do
    DashboardTimestampService.new(user)
  end

  it '#initialize' do
    expect { ds }.not_to raise_error
    expect(ds.user).to eq user
    expect(user.dashboard_timestamps).to be_empty
  end

  it '#timestamps' do
    expect(ds.timestamps).to be_a Hash
  end

  it '#timestamp_for' do
    expect(ds.timestamp_for('test')).to be_a Time
  end

  it '#mark_as_read' do
    old = ds.timestamp_for('test')
    expect { ds.mark_as_read('test') }.not_to raise_error
    new = ds.timestamp_for('test')
    expect(new).to be > old
  end

  it '#mark_all_as_read' do
    old1 = ds.timestamp_for('test')
    old2 = ds.timestamp_for('bla')
    expect { ds.mark_as_read('all') }.not_to raise_error
    new1 = ds.timestamp_for('test')
    new2 = ds.timestamp_for('bla')
    expect(new1).to be > old1
    expect(new2).to be > old2
    expect(new2).to eq ds.timestamp_for('unknown')
  end

  context 'without user' do
    let(:user) { nil }

    it '#timestamps' do
      expect(ds.timestamps).to be_empty
    end

    it '#timestamp_for' do
      expect(ds.timestamp_for('test')).to be_a Time
    end

    it '#mark_as_read' do
      expect { ds.mark_as_read('test') }.not_to raise_error
    end

    it '#mark_all_as_read' do
      expect { ds.mark_all_as_read }.not_to raise_error
    end
  end
end
