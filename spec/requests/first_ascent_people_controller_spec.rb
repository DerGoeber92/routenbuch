require 'rails_helper'

RSpec.describe FirstAscentPeopleController, type: :request do
  login_user :admin_user

  before(:each) do
    @first_ascent_person = create(:first_ascent_person)
    create(:route, first_ascent_person: @first_ascent_person)
  end

  it 'index' do
    get first_ascent_people_path
    expect(response).to have_http_status(:ok)
  end

  it 'show' do
    get first_ascent_person_path(@first_ascent_person)
    expect(response).to have_http_status(:ok)
  end

  it 'edit' do
    get edit_first_ascent_person_path(@first_ascent_person)
    expect(response).to have_http_status(:ok)
  end

  it 'update' do
    patch(
      first_ascent_person_path(@first_ascent_person),
      params: {
        first_ascent_person: {
          first_name: 'update',
          last_name: 'me!'
        }
      }
    )
    expect(response).to have_http_status(:found)
    @first_ascent_person.reload
    expect(@first_ascent_person.first_name).to eq('update')
    expect(@first_ascent_person.last_name).to eq('me!')
  end

  it 'destroy' do
    delete first_ascent_person_path(@first_ascent_person)
    expect(response).to have_http_status(:found)
    expect(FirstAscentPerson.exists? @first_ascent_person.id).to be false
  end
end
