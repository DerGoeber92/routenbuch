module AbilityPresenters
  class TablePresenter
    attr_reader :users
    attr_reader :actions
    attr_reader :table
    attr_reader :head

    def initialize
      @users = []
      @actions = []
      @permissions = []

      yield self

      generate
    end

    def add_user(label, user)
      @users << {
        label: label,
        user: user,
        ability: Ability.new(user)
      }
    end

    def add_action(label, action, object)
      @actions << {
        label: label,
        action: action,
        object: object
      }
    end

    def add_user_owner
      @owner = User.new(id: 100, role: 'member')
      add_user(_('Member (owner)'), @owner)
    end

    def add_users_with_roles
      add_user(_('Guest'), User.new(role: 'guest'))
      add_user(_('Member'), User.new(role: 'member'))
      add_user(_('Contributor'), User.new(role: 'contributor'))
      add_user(_('Admin'), User.new(role: 'admin'))
    end

    def add_contributor_users
      id = 0
      %w[editor coordinator manager].each do |level|
        id += 1
        user = User.new(
          id: id,
          role: 'contributor'
        )
        @permissions << Permission.new(
          user: user,
          level: level
        )
        add_user(_('Contributor with %{level} permission') % { level: level }, user)
      end
    end

    def add_geo_ref_actions
      %w[public internal private confidential].each do |access|
        parent = GeoRef.new(
          access: access,
        )
        parent.effective_permissions = @permissions
        geo_ref = GeoRef.new(
          parent: parent,
          access: access,
        )
        geo_ref.effective_permissions = @permissions

        add_action(_('View %{access} location') % {access: access} , :read, geo_ref)
        add_action(_('Create %{access} location') % {access: access} , :create, geo_ref)
        add_action(_('Update %{access} location') % {access: access} , :update, geo_ref)
        add_action(_('Delete %{access} location') % {access: access} , :destroy, geo_ref)
      end
    end

    def add_comments_actions
      %w[public internal private confidential].each do |access|
        geo_ref = GeoRef.new(
          access: access,
        )
        geo_ref.effective_permissions = @permissions
        comment = Comment.new(
          target: geo_ref,
          user: @owner
        )

        add_action(_('View comment of %{access} location') % {access: access} , :read, comment)
        add_action(_('Create comment of %{access} location') % {access: access} , :create, comment)
        add_action(_('Update comment of %{access} location') % {access: access} , :update, comment)
        add_action(_('Delete comment of %{access} location') % {access: access} , :destroy, comment)
      end
    end

    def generate
      @head = @users.map {|u| u[:label]}
      @table = @actions.map do |action|
        @row = []
        @row << action[:label]
        @users.each do |user|
          @row << user[:ability].can?(action[:action], action[:object])
        end
        @row
      end
    end
  end
end
