FactoryBot.define do
  factory :rating do
    target factory: :route
    after(:create) do |object, _evaluator|
      create(:user_rating, rating: object, score: 3)
      create(:user_rating, rating: object, score: 4)
      create(:user_rating, rating: object, score: 5)
    end
  end
end
