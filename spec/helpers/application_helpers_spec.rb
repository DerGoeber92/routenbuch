require "rails_helper"

RSpec.describe ApplicationHelper do
  let(:icon_mock) do
    mock = double('SomeModel')
    allow(mock).to receive(:icon).and_return('apple')
    return mock
  end

  it '#icon_for with string' do
    expect(helper.icon_for('lock')).to eq '<i class="fa fa-lock" aria-hidden="true"></i>'
  end

  it '#icon_for with object' do
    expect(helper.icon_for(icon_mock)).to eq '<i class="fa fa-apple" aria-hidden="true"></i>'
  end

  it '#icon_link_to' do
    route = create(:route)
    expect { helper.icon_link_to(route, 'my route', route) }.to_not raise_error
  end
end
