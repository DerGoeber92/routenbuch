# == Schema Information
#
# Table name: photos
#
#  id          :integer          not null, primary key
#  cover_photo :boolean          default(FALSE), not null
#  description :string
#  pinned      :boolean          default(FALSE), not null
#  private     :boolean          default(FALSE), not null
#  target_type :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  geo_ref_id  :bigint           not null
#  target_id   :integer
#  user_id     :bigint           not null
#
# Indexes
#
#  index_photos_on_cover_photo                (cover_photo)
#  index_photos_on_geo_ref_id                 (geo_ref_id)
#  index_photos_on_pinned                     (pinned)
#  index_photos_on_private                    (private)
#  index_photos_on_target_type_and_target_id  (target_type,target_id)
#  index_photos_on_user_id                    (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#  fk_rails_...  (user_id => users.id)
#
class PhotoSerializer
  include JSONAPI::Serializer

  set_type :photo
  set_id :id
  
  attributes :description

  attribute :url do |object|
    object.photo.url
  end

  attribute :thumb_url do |object|
    object.photo.thumb.url
  end

  attribute :created_at do |object|
    object.created_at.strftime('%Y-%m-%d')
  end
end
