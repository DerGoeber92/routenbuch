class AddClosureReasons < ActiveRecord::Migration[6.1]
  def change
    create_table :closure_reasons do |t|
      t.string :name, null: false
    end

    add_index :closure_reasons, :name, unique: true
    add_reference :closures, :closure_reason, foreign_key: true

    remove_column :closure_templates, :closure_name, :string, default: '', null: false
    add_reference :closure_templates, :closure_reason, foreign_key: true
  end
end
