class AddAccessToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :scope_web_ui, :boolean, null: false, default: true
    add_column :users, :scope_embed_api, :boolean, null: false, default: false
  end
end
