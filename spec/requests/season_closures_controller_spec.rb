require 'rails_helper'

RSpec.describe SeasonClosuresController, type: :request do
  login_user :admin_user

  before(:each) do
    @season_closure = create(:season_closure)
  end

  it 'new' do
    get new_closure_season_closure_path(@season_closure.closure)
    expect(response).to have_http_status(:ok)
  end

  it 'create' do
    post(
      closure_season_closures_path(create(:closure)),
      params: {
        season_closure: {
          description: 'my season closure',
          year: 2021
        }
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("SeasonClosure was successfully created.")
  end

  it 'edit' do
    get edit_season_closure_path(@season_closure)
    expect(response).to have_http_status(:ok)
  end

  it 'update' do
    patch(
      season_closure_path(@season_closure),
      params: {
        season_closure: {
          description: 'update me!'
        }
      }
    )
    expect(response).to have_http_status(:found)
    @season_closure.reload
    expect(@season_closure.description).to eq('update me!')
    follow_redirect!
    expect(response.body).to include("SeasonClosure was successfully updated.")
  end

  it 'destroy' do
    delete season_closure_path(@season_closure)
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("SeasonClosure was successfully destroyed.")
    expect(SeasonClosure.exists? @season_closure.id).to be false
  end
end
