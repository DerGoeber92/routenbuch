FactoryBot.define do
  factory :inventory do
    route
    product
    anchor_number { 1 }
  end
end
