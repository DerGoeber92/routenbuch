class AddDirectRelationOnGeoRefs < ActiveRecord::Migration[6.1]
  def change
    add_reference :comments, :geo_ref, foreign_key: true
    add_reference :photos, :geo_ref, foreign_key: true

    up_only do
      [Comment, Photo].each do |klass|
        klass.all.each do |item|
          if item.target.is_a? GeoRef
            item.update(geo_ref_id: item.target.id)
          else
            item.update(geo_ref_id: item.target.geo_ref.id)
          end
        end
      end

      change_column_null :comments, :geo_ref_id, false
      change_column_null :photos, :geo_ref_id, false
    end
  end
end
