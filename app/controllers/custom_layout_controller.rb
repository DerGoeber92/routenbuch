class CustomLayoutController < ApplicationController
  skip_authorization_check only: [:index]
  skip_before_action(
    :authenticate_user!,
    only: [:index]
  ) unless Routenbuch.public?
  back_navigatable_actions []

  def index
    render template: 'custom_layout/config', formats: [:css]
  end
end
