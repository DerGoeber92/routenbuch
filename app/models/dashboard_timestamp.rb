# == Schema Information
#
# Table name: dashboard_timestamps
#
#  id         :bigint           not null, primary key
#  kind       :string           not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_dashboard_timestamps_on_user_id           (user_id)
#  index_dashboard_timestamps_on_user_id_and_kind  (user_id,kind) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class DashboardTimestamp < ApplicationRecord
  validates :user, presence: true
  validates(
    :kind,
    presence: true,
    inclusion: { in: %w[geo_refs routes comments all] }
  )
  validates :updated_at, presence: true
end
