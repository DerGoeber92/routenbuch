# == Schema Information
#
# Table name: closure_reasons
#
#  id   :bigint           not null, primary key
#  name :string           not null
#
# Indexes
#
#  index_closure_reasons_on_name  (name) UNIQUE
#
class ClosureReason < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  def self.ransackable_attributes(_auth_object = nil)
    %w[name]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end
end
