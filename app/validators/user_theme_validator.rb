class UserThemeValidator < ActiveModel::Validations::InclusionValidator
  class << self
    THEMES = %w[classic twentytwo].freeze

    def themes
      THEMES
    end

    def valid?(theme)
      THEMES.include?(theme)
    end
  end

  def options
    { in: self.class.themes }
  end
end
