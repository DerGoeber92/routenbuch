module RbidIdentifiable
  extend ActiveSupport::Concern

  included do
    has_many :rbids, as: :item

    scope(
      :without_rbid,
      lambda do
        left_outer_joins(:rbids).where(rbids: { id: nil })
      end
    )

    after_create :create_rbid!

    def create_rbid!
      hint = if respond_to? :name
               name
             else
               ''
             end
      Rbid.create!(
        item: self,
        hint: hint
      )
    end
  end
end
