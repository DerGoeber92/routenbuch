require 'rails_helper'

RSpec.describe ErrorsController, type: :request do
  login_user :admin_user

  it 'forbidden' do
    get '/403'
    expect(response).to have_http_status(:ok)
  end

  it 'not_found' do
    get '/404'
    expect(response).to have_http_status(:ok)
  end

  it 'unprocessable_entity' do
    get '/422'
    expect(response).to have_http_status(:ok)
  end

  it 'internal_server_error' do
    get '/500'
    expect(response).to have_http_status(:ok)
  end
end
