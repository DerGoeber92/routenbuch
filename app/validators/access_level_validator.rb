class AccessLevelValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    raise ArgumentError, 'record does not support access_levels' unless record.respond_to?(:is_valid_access_level?)

    record.errors[attribute] << 'is not a valid access_level' unless AccessLevelType.is_valid?(value)

    record.errors[attribute] << (options[:message] || "is not a valid access level for this object") \
      unless record.is_valid_access_level?(value)
  end
end
