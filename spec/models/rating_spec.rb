require 'rails_helper'

RSpec.describe Rating, type: :model do
  let(:attributes) { {} }
  let(:factory) { :rating }
  let(:rating) { create factory, **attributes }

  context 'object creation' do
    it 'validation' do
      expect { rating.validate! }.not_to raise_error
    end

    it 'attributes' do
      expect(rating.average_score).to eq 4.0
      expect(rating.user_ratings_count).to eq 3
    end

    it '#update_average_score' do
      rating.update(average_score: nil)
      expect(rating.average_score).to be nil
      expect { rating.update_average_score }.not_to raise_error
      expect(rating.average_score).to eq 4.0
    end
  end

  context 'model class' do
    let(:route1) do
      create(:route, name: 'route 1', rating: create(:rating))
    end
    let(:route2) do
      create(:route, name: 'route 2', rating: create(:rating))
    end
    let(:relation) do
      Rating.where(id: [route1.rating.id, route2.rating.id])
    end

    it 'update average_scores on relation' do
      expect(relation.count).to eq 2
      expect(route1.rating.average_score).to eq 4.0
      relation.update_all(average_score: nil)
      route1.reload
      expect(route1.rating.average_score).to be nil
      expect { relation.update_average_scores }.not_to raise_error
      route1.reload
      expect(route1.rating.average_score).to eq 4.0
    end
  end
end
