class AddUniqueIndexToAssignedTags < ActiveRecord::Migration[7.0]
  def change
    add_index :assigned_tags, %i[taggable_type taggable_id tag_id], unique: true
  end
end
