# == Schema Information
#
# Table name: routes
#
#  id                     :integer          not null, primary key
#  alternative_names      :string           default([]), not null, is an Array
#  body                   :text
#  description            :string
#  first_ascent_day       :integer
#  first_ascent_month     :integer
#  first_ascent_year      :integer
#  legal_status           :string
#  name                   :string
#  pitches                :integer
#  priority               :integer          default(0), not null
#  searchable_terms       :string           default(""), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  first_ascent_person_id :integer
#  geo_ref_id             :integer
#  grade_id               :integer
#
# Indexes
#
#  index_routes_on_first_ascent_person_id  (first_ascent_person_id)
#  index_routes_on_geo_ref_id              (geo_ref_id)
#  index_routes_on_grade_id                (grade_id)
#  index_routes_on_legal_status            (legal_status)
#  index_routes_on_priority                (priority)
#
# Foreign Keys
#
#  fk_rails_...  (first_ascent_person_id => first_ascent_people.id)
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#  fk_rails_...  (grade_id => grades.id)
#
require 'rails_helper'

RSpec.describe Route, type: :model do
  let(:attributes) { {} }
  let(:route) { create :route, **attributes }

  context 'route' do
    it 'validation' do
      expect { route.validate! }.not_to raise_error
    end

    it 'rbid' do
      expect(route.rbids.first).to be_a Rbid
    end
  end
end
