# == Schema Information
#
# Table name: secondary_zones
#
#  id          :bigint           not null, primary key
#  description :string           default(""), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  geo_ref_id  :bigint           not null
#  zone_id     :bigint           not null
#
# Indexes
#
#  index_secondary_zones_on_geo_ref_id  (geo_ref_id)
#  index_secondary_zones_on_zone_id     (zone_id)
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#  fk_rails_...  (zone_id => zones.id)
#
class SecondaryZone < ApplicationRecord
  belongs_to :geo_ref
  belongs_to :zone

  validates :description, presence: true

  def self.valid_geo_ref_types
    [Crag, Sector]
  end

  def self.valid_geo_ref_type?(klass)
    valid_geo_ref_types.include?(klass)
  end

  validate :validate_geo_ref_type
  def validate_geo_ref_type
    return if self.class.valid_geo_ref_type?(geo_ref.class)

    errors.add(:geo_ref, _('must be an allowed location type.'))
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[description]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end
end
