class CreateVersions < ActiveRecord::Migration[6.1]
  def change
    create_table :versions do |t|
      t.string   :item_type, { null: false }
      t.bigint   :item_id,   null: false
      t.string   :event,     null: false
      t.string   :whodunnit
      t.jsonb    :object
      t.jsonb    :object_changes

      t.datetime :created_at

      # add references to the geo_ref/route objects to localize changes
      # within the tree
      t.references :geo_ref, null: false, foreign_key: { on_delete: :cascade }
      t.references :route, foreign_key: { on_delete: :nullify }

      # direct reference to the user if available
      t.references :user, foreign_key: { on_delete: :nullify }
    end
    add_index :versions, %i(item_type item_id)
  end
end
