class ZonesController < ApplicationController
  def index
    @zones = Zone.all
    authorize! :read, @zones
  end
end
