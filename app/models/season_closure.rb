# == Schema Information
#
# Table name: season_closures
#
#  id                 :bigint           not null, primary key
#  description        :string
#  end_day_of_month   :integer
#  end_month          :integer
#  start_day_of_month :integer
#  start_month        :integer
#  year               :integer          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  closure_id         :bigint           not null
#
# Indexes
#
#  index_season_closures_on_closure_id           (closure_id)
#  index_season_closures_on_closure_id_and_year  (closure_id,year) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (closure_id => closures.id)
#
class SeasonClosure < ApplicationRecord
  belongs_to :closure

  validates :description, presence: true
  validates(
    :year,
    numericality: {
      greater_than_or_equal_to: 2000,
      less_than_or_equal_to: 2100
    },
    uniqueness: {
      scope: :closure,
      message: _('A season closure for this year already exists')
    }
  )
  validates(
    :start_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  )
  validates(
    :start_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  )
  validates(
    :end_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  )
  validates(
    :end_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  )

  def self.icon
    'lock'
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[description]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end

  after_save :touch_closure_cache
  after_destroy :touch_closure_cache
  def touch_closure_cache
    # trigger update of cached values
    closure.reset_calculations_cache
    closure.save!
  end

  has_paper_trail(
    meta: {
      geo_ref_id: proc { |sc| sc.closure.geo_ref_id }
    }
  )
end
