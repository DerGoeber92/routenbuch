# == Schema Information
#
# Table name: paths
#
#  id         :bigint           not null, primary key
#  access     :string           default("private"), not null
#  body       :string           default(""), not null
#  name       :string           not null
#  type       :string           default("Approach"), not null
#  geo_ref_id :bigint
#
# Indexes
#
#  index_paths_on_geo_ref_id  (geo_ref_id)
#  index_paths_on_type        (type)
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#
class Path < ApplicationRecord
  class << self
    def icon
      'map-o'
    end

    def path_types
      %w[Approach].freeze
    end
  end

  belongs_to :geo_ref
  has_and_belongs_to_many :geo_refs

  validates :name, presence: true

  validates(
    :type,
    inclusion: {
      in: Path.path_types,
      message: '%{value} is not a valid path type!'
    }
  )

  validate :validate_geo_ref
  def validate_geo_ref
    unless geo_ref.type == 'Parking'
      errors.add(:geo_ref, 'Must be of type Parking')
    end
  end

  has_one_attached :track

  include Taggable
  include RbidIdentifiable

  def self.ransackable_attributes(_auth_object = nil)
    %w[name type body]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[geo_ref geo_refs]
  end

  has_paper_trail(
    meta: {
      geo_ref_id: :geo_ref_id
    }
  )
end
