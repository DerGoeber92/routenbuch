class CreateClosureTemplates < ActiveRecord::Migration[6.1]
  def change
    create_table :closure_templates do |t|
      t.string :name, null: false
      t.string :description, default: '', null: false
      t.integer :regular_start_month
      t.integer :regular_start_day_of_month
      t.integer :regular_end_month
      t.integer :regular_end_day_of_month

      t.timestamps
    end
  end
end
