class LegalStatusType < ActiveRecord::Type::Value
  class << self
    VALID_VALUES = [
      N_('approved'),
      N_('declined'),
      N_('requested'),
      N_('unknown')
    ].freeze

    def valid_values
      VALID_VALUES
    end

    def is_valid?(value)
      VALID_VALUES.include?(value)
    end
  end

  def type
    :legal_status
  end

  def cast(value)
    return nil if value.blank?
    return nil unless self.class.is_valid?(value)

    value
  end
end
