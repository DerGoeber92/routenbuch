class CreateWaysTables < ActiveRecord::Migration[6.0]
  def change
    create_table :ways do |t|
      t.string :name, null: false
      t.string :type, null: false, default: 'Approach'
      t.string :body, null: false, default: ''
      t.string :access, null: false, default: 'private'
      t.references :geo_ref, foreign_key: true
    end

    add_index :ways, :type

    create_table :geo_refs_ways, id: false, force: :cascade do |t|
      t.references :geo_ref, foreign_key: true
      t.references :way, foreign_key: true
    end
  end
end
