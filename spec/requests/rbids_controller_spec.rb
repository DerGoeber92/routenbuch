require 'rails_helper'

RSpec.describe RbidsController, type: :request do
  login_user :admin_user

  before(:each) do
    @rbid = create(:rbid)
  end

  it 'show' do
    get rbid_path(@rbid)
    expect(response).to have_http_status(:ok)
  end

  it 'qrcode' do
    get qrcode_rbid_path(@rbid, format: :svg)
    expect(response).to have_http_status :ok
    expect(response.media_type).to eq 'image/svg+xml'
  end

  it 'redirect' do
    get rbid_redirect_path(@rbid.rbid)
    expect(response).to have_http_status(:found)
    expect(response.headers['Location']).to eq polymorphic_url(@rbid.item.as_base_class)
  end
end
