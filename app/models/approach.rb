# == Schema Information
#
# Table name: paths
#
#  id         :bigint           not null, primary key
#  access     :string           default("private"), not null
#  body       :string           default(""), not null
#  name       :string           not null
#  type       :string           default("Approach"), not null
#  geo_ref_id :bigint
#
# Indexes
#
#  index_paths_on_geo_ref_id  (geo_ref_id)
#  index_paths_on_type        (type)
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#
class Approach < Path
end
